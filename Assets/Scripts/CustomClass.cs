﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringItem
{
	/// <summary>
	/// 
	/// </summary>
	public string Key { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public string Value { get; set; }
}

public class DictionaryItem
{
	/// <summary>
	/// 
	/// </summary>
	public string Language { get; set; }
	/// <summary>
	/// 
	/// </summary>
	public List<StringItem> StringItem { get; set; }
}

public class Dictionaries
{
	/// <summary>
	/// 
	/// </summary>
	public List<DictionaryItem> DictionaryItem { get; set; }
}

public class Root
{
	/// <summary>
	/// 
	/// </summary>
	public Dictionaries Dictionaries { get; set; }
}
