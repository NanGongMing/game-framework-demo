﻿using GameFramework.Event;
using GameFramework.Procedure;
using UnityGameFramework.Runtime;
using ProcedureOwner = GameFramework.Fsm.IFsm<GameFramework.Procedure.IProcedureManager>;
using System;

namespace GameFrameworkExample
{
	public class ProcedureMenu : ProcedureBase
	{


		protected override void OnEnter(ProcedureOwner procedureOwner)
		{
			base.OnEnter(procedureOwner);
			Log.Error($"当前流程:{GetType()}");


			Component.Event.Subscribe(OpenUIFormSuccessEventArgs.EventId, OpenUIFormSuccess);
			Component.UI.OpenUIForm("Assets/Prefabs/Canvas_Menu.prefab", "Default", "打开UI成功事件");

		}
		protected override void OnUpdate(ProcedureOwner procedureOwner, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(procedureOwner, elapseSeconds, realElapseSeconds);
		}

		protected override void OnLeave(ProcedureOwner procedureOwner, bool isShutdown)
		{
			base.OnLeave(procedureOwner, isShutdown);
			Component.Event.Unsubscribe(OpenUIFormSuccessEventArgs.EventId, OpenUIFormSuccess);

		}
		void OpenUIFormSuccess(object sender, GameEventArgs e)
		{
			OpenUIFormSuccessEventArgs openUIFormSuccessEventArgs = (OpenUIFormSuccessEventArgs)e;
			Log.Error($"{openUIFormSuccessEventArgs.UserData}:{DateTime.Now}");
		}
	}
}
