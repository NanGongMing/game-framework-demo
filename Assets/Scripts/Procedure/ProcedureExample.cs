﻿using GameFramework.Procedure;
using UnityGameFramework.Runtime;
using ProcedureOwner = GameFramework.Fsm.IFsm<GameFramework.Procedure.IProcedureManager>;
using UnityEngine;
using GameFramework.Event;
using System;
using GameFramework;
using Version = GameFramework.Version;
using GameFramework.Localization;
using System.Collections;

namespace GameFrameworkExample
{
	public class ProcedureExample : ProcedureBase
	{


		protected override void OnEnter(ProcedureOwner procedureOwner)
		{
			base.OnEnter(procedureOwner);

			//string welcomeMessage = Utility.Text.Format("Hello! This is an empty project based on Game Framework {0}.", Version.GameFrameworkVersion);
			//Log.Info(welcomeMessage);
			//Log.Warning(welcomeMessage);
			//Log.Error(welcomeMessage);

			Log.Error("This is a demo,Press Space to go to the next procedure");
			Log.Error($"当前流程:{GetType()}");


			Component.Event.Subscribe(KeyCodePressEventArgs.EventId, KeyCodePress);
			InitLanguageSettings();
			InitSoundSettings();

		}
		protected override void OnUpdate(ProcedureOwner procedureOwner, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(procedureOwner, elapseSeconds, realElapseSeconds);

			if (Input.anyKeyDown)
			{
				foreach (KeyCode keyCode in Enum.GetValues(typeof(KeyCode)))
				{
					if (Input.GetKeyDown(keyCode))
					{
						Component.Event.Fire(this, KeyCodePressEventArgs.Create(keyCode, procedureOwner));
					}
				}
			}
		}

		protected override void OnLeave(ProcedureOwner procedureOwner, bool isShutdown)
		{
			base.OnLeave(procedureOwner, isShutdown);

			Component.Event.Unsubscribe(KeyCodePressEventArgs.EventId, KeyCodePress);
		}
		void KeyCodePress(object sender, GameEventArgs e)
		{
			KeyCodePressEventArgs keyCodePressEventArgs = (KeyCodePressEventArgs)e;
			Log.Error($"{keyCodePressEventArgs.KeyCode}被按下");
			if (KeyCode.Space == keyCodePressEventArgs.KeyCode)
			{
				ChangeState<ProcedurePreload>((ProcedureOwner)keyCodePressEventArgs.UserData);


			}
		}
		//初始化语言设置
		private void InitLanguageSettings()
		{
			if (Component.Base.EditorResourceMode && Component.Base.EditorLanguage != Language.Unspecified)
			{
				// 编辑器资源模式直接使用 Inspector 上设置的语言
				return;
			}

			Language language = Component.Localization.Language;
			if (Component.Setting.HasSetting(Constant.Language))
			{
				try
				{
					string languageString = Component.Setting.GetString(Constant.Language);
					language = (Language)Enum.Parse(typeof(Language), languageString);
				}
				catch
				{
				}
			}

			if (language != Language.English
				&& language != Language.ChineseSimplified)
			{
				// 若是暂不支持的语言，则使用中文
				language = Language.ChineseSimplified;

				Component.Setting.SetString(Constant.Language, language.ToString());
				Component.Setting.Save();
			}

			Component.Localization.Language = language;
		}
		private void InitSoundSettings()
		{
			Component.Sound.Mute("Music", Component.Setting.GetBool(Constant.MusicMuted, false));
			Component.Sound.SetVolume("Music", Component.Setting.GetFloat(Constant.MusicVolume, 0.3f));
			Component.Sound.Mute("SoundEffect", Component.Setting.GetBool(Constant.SoundEffectMuted, false));
			Component.Sound.SetVolume("SoundEffect", Component.Setting.GetFloat(Constant.SoundEffectVolume, 1f));
			Component.Sound.Mute("UISoundEffect", Component.Setting.GetBool(Constant.UISoundEffectMuted, false));
			Component.Sound.SetVolume("UISoundEffect", Component.Setting.GetFloat(Constant.UISoundEffectVolume, 1f));
		}
	}
}
