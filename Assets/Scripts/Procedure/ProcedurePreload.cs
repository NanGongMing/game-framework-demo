﻿using GameFramework.Procedure;
using UnityGameFramework.Runtime;
using ProcedureOwner = GameFramework.Fsm.IFsm<GameFramework.Procedure.IProcedureManager>;
using UnityEngine;
using GameFramework.Event;
using System;
using GameFramework;
using Version = GameFramework.Version;
using System.Collections.Generic;

namespace GameFrameworkExample
{
	public class ProcedurePreload : ProcedureBase
	{

		private Dictionary<string, bool> m_LoadedFlag = new Dictionary<string, bool>();

		protected override void OnEnter(ProcedureOwner procedureOwner)
		{
			base.OnEnter(procedureOwner);
			Component.Event.Subscribe(LoadDictionarySuccessEventArgs.EventId, OnLoadDictionarySuccess);
			Component.Event.Subscribe(LoadDictionaryFailureEventArgs.EventId, OnLoadDictionaryFailure);
			m_LoadedFlag.Clear();
			PreloadResources();

		}
		protected override void OnUpdate(ProcedureOwner procedureOwner, float elapseSeconds, float realElapseSeconds)
		{
			base.OnUpdate(procedureOwner, elapseSeconds, realElapseSeconds);
			foreach (KeyValuePair<string, bool> loadedFlag in m_LoadedFlag)
			{
				if (!loadedFlag.Value)
				{
					return;
				}
			}
			ChangeState<ProcedureMenu>(procedureOwner);

		}

		protected override void OnLeave(ProcedureOwner procedureOwner, bool isShutdown)
		{
			base.OnLeave(procedureOwner, isShutdown);
			Component.Event.Unsubscribe(LoadDictionarySuccessEventArgs.EventId, OnLoadDictionarySuccess);
			Component.Event.Unsubscribe(LoadDictionaryFailureEventArgs.EventId, OnLoadDictionaryFailure);
		}
		private void PreloadResources()
		{
			LoadDictionary("Default");
		}
		private void LoadDictionary(string dictionaryName)
		{
			string dictionaryAssetName = Utility.Text.Format("Assets/Localization/{0}/Dictionaries/{1}.txt", Component.Localization.Language.ToString(), dictionaryName);

			m_LoadedFlag.Add(dictionaryAssetName, false);
			Component.Localization.ReadData(dictionaryAssetName, this);
		}

		private void OnLoadDictionarySuccess(object sender, GameEventArgs e)
		{
			LoadDictionarySuccessEventArgs ne = (LoadDictionarySuccessEventArgs)e;
			if (ne.UserData != this)
			{
				return;
			}

			m_LoadedFlag[ne.DictionaryAssetName] = true;
			//Log.Info("加载Localization字典'{0}' 成功.", ne.DictionaryAssetName);
			//Log.Error("Localization测试信息:Key: Menu.StartButton Value: " + Component.Localization.GetString("Menu.StartButton"));
			//Log.Error("Localization测试信息:Key: Menu.SettingButton Value: " + Component.Localization.GetString("Menu.SettingButton"));
			//Log.Error("Localization测试信息:Key: Language.English Value: " + Component.Localization.GetString("Language.English"));


		}
		private void OnLoadDictionaryFailure(object sender, GameEventArgs e)
		{
			LoadDictionaryFailureEventArgs ne = (LoadDictionaryFailureEventArgs)e;
			if (ne.UserData != this)
			{
				return;
			}

			Log.Error("加载Localization字典失败'{0}' 错误信息 '{2}'.", ne.DictionaryAssetName, ne.ErrorMessage);
		}
	}
}
