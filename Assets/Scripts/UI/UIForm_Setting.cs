﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;
using UnityEngine.UI;
using GameFramework.Localization;
using GameFramework.Sound;
using GameFramework;

public class UIForm_Setting : GFUIForm
{
	public Button Btn_Confirm;
	public Button Btn_Cancel;


	public Toggle MusicToggle;
	public Slider MusicVolume;
	public Toggle SoundEffectToggle;
	public Slider SoundEffectVolume;
	public Toggle UISoundEffectToggle;
	public Slider UISoundEffectVolume;

	public Toggle EnglishToggle = null;
	public Text Text_LanguageTips;
	public Toggle ChineseSimplifiedToggle = null;
	private Language m_SelectedLanguage = Language.Unspecified;

	/// <summary>
	/// 界面初始化
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnInit(object userData)
	{
		base.OnInit(userData);

		Btn_Cancel.onClick.AddListener(() =>
		{
			InitSoundSettings();

			Component.UI.OpenUIForm("Assets/Prefabs/Canvas_Menu.prefab", "Default");
			Close();
		});

	}

	void InitSoundSettings()
	{
		MusicToggle.isOn = !Component.Setting.GetBool(Constant.MusicMuted, true);
		MusicVolume.value = Component.Setting.GetFloat(Constant.MusicVolume, 0.3f);

		SoundEffectToggle.isOn = !Component.Setting.GetBool(Constant.SoundEffectMuted, true);
		SoundEffectVolume.value = Component.Setting.GetFloat(Constant.SoundEffectVolume, 1f);

		UISoundEffectToggle.isOn = !Component.Setting.GetBool(Constant.UISoundEffectMuted, true);
		UISoundEffectVolume.value = Component.Setting.GetFloat(Constant.UISoundEffectVolume, 1f);
	}

	/// <summary>
	/// 界面打开
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnOpen(object userData)
	{
		base.OnOpen(userData);

		InitSoundSettings();

		m_SelectedLanguage = Component.Localization.Language;
		switch (m_SelectedLanguage)
		{
			case Language.English:
				EnglishToggle.isOn = true;
				break;
			case Language.ChineseSimplified:
				ChineseSimplifiedToggle.isOn = true;
				break;
			default:
				break;
		}
	}


	/// <summary>
	/// 界面关闭
	/// </summary>
	/// <param name="isShutdown"></param>
	/// <param name="userData"></param>
	protected override void OnClose(bool isShutdown, object userData)
	{
		base.OnClose(isShutdown, userData);
	}

	public void OnMusicToggleChanged(bool isOn)
	{
		Component.Sound.Mute("Music", !isOn);
		MusicVolume.gameObject.SetActive(isOn);

	}

	public void OnMusicVolumeChanged(float volume)
	{
		Component.Sound.SetVolume("Music", volume);
	}

	public void OnSoundToggleChanged(bool isOn)
	{
		Component.Sound.Mute("SoundEffect", !isOn);
		SoundEffectVolume.gameObject.SetActive(isOn);
	}

	public void OnSoundVolumeChanged(float volume)
	{
		Component.Sound.SetVolume("SoundEffect", volume);

	}

	public void OnUISoundToggleChanged(bool isOn)
	{
		Component.Sound.Mute("UISoundEffect", !isOn);
		UISoundEffectVolume.gameObject.SetActive(isOn);
	}

	public void OnUISoundVolumeChanged(float volume)
	{
		Component.Sound.SetVolume("UISoundEffect", volume);

	}
	public void OnEnglishSelected(bool isOn)
	{
		if (!isOn)
		{
			return;
		}

		m_SelectedLanguage = Language.English;
		RefreshLanguageTips();
	}

	public void OnChineseSimplifiedSelected(bool isOn)
	{
		if (!isOn)
		{
			return;
		}

		m_SelectedLanguage = Language.ChineseSimplified;
		RefreshLanguageTips();
	}
	private void RefreshLanguageTips()
	{
		Text_LanguageTips.gameObject.SetActive(m_SelectedLanguage != Component.Localization.Language);
	}
	public void ClickConfirmBtn()
	{


		Component.Setting.SetBool(Constant.MusicMuted, !MusicToggle.isOn);
		Component.Setting.SetFloat(Constant.MusicVolume, MusicVolume.value);

		Component.Setting.SetBool(Constant.SoundEffectMuted, !SoundEffectToggle.isOn);
		Component.Setting.SetFloat(Constant.SoundEffectVolume, SoundEffectVolume.value);

		Component.Setting.SetBool(Constant.UISoundEffectMuted, !UISoundEffectToggle.isOn);
		Component.Setting.SetFloat(Constant.UISoundEffectVolume, UISoundEffectVolume.value);

		Component.Setting.SetString(Constant.Language, m_SelectedLanguage.ToString());

		Component.Setting.Save();

		if (m_SelectedLanguage == Component.Localization.Language)
		{
			Close();
			Component.UI.OpenUIForm("Assets/Prefabs/Canvas_Menu.prefab", "Default");
			return;
		}
		GameEntry.Shutdown(ShutdownType.Restart);
	}

}
