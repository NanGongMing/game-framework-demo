﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;
using UnityEngine.UI;
using GameFramework.Sound;

public class GFUIForm : UIFormLogic
{


	/// <summary>
	/// 界面初始化
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnInit(object userData)
	{
		base.OnInit(userData);

		Text[] texts = GetComponentsInChildren<Text>(true);
		for (int i = 0; i < texts.Length; i++)
		{
			if (!string.IsNullOrEmpty(texts[i].text))
			{
				texts[i].text = Component.Localization.GetString(texts[i].text);
			}
		}

	}
	/// <summary>
	/// 界面打开
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnOpen(object userData)
	{
		base.OnOpen(userData);


	}
	public void PlayUISound()
	{
		Component.Sound.PlayUISound("uisoundeffect");
	}
	/// <summary>
	/// 界面关闭
	/// </summary>
	/// <param name="isShutdown"></param>
	/// <param name="userData"></param>
	protected override void OnClose(bool isShutdown, object userData)
	{
		base.OnClose(isShutdown, userData);


	}
	public void Close()
	{
		Component.UI.CloseUIForm(this.UIForm);
	}
}
