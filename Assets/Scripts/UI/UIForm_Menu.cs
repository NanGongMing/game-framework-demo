﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGameFramework.Runtime;
using UnityEngine.UI;
using GameFramework.Sound;
using System;

public class UIForm_Menu : GFUIForm
{
	public Button Btn_StartGame;
	public Button Btn_Setting;

	/// <summary>
	/// 界面初始化
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnInit(object userData)
	{
		base.OnInit(userData);
		Component.Sound.PlayMusic("bgm");

		Btn_Setting.onClick.AddListener(() =>
		{
			Component.UI.OpenUIForm("Assets/Prefabs/Canvas_Setting.prefab", "Default");
			Close();
		});
		Btn_StartGame.onClick.AddListener(() =>
		{
			Component.Scene.LoadScene("Assets/Scenes/Game.unity");
			Close();
		});
	}
	/// <summary>
	/// 界面打开
	/// </summary>
	/// <param name="userData"></param>
	protected override void OnOpen(object userData)
	{
		base.OnOpen(userData);


	}


	/// <summary>
	/// 界面关闭
	/// </summary>
	/// <param name="isShutdown"></param>
	/// <param name="userData"></param>
	protected override void OnClose(bool isShutdown, object userData)
	{
		base.OnClose(isShutdown, userData);


	}
}
