﻿using GameFramework;
using GameFramework.Localization;
using LitJson;
using System;
using UnityGameFramework.Runtime;

public class JsonLocalizationHelper : DefaultLocalizationHelper
{
	public override bool ParseData(ILocalizationManager localizationManager, string dictionaryString, object userData)
	{
		try
		{
			string currentLanguage = Component.Localization.Language.ToString();

			//GF内置的Json解析不出来,暂未深究其原因
			//Root root = Utility.Json.ToObject<Root>(dictionaryString);
			Root root = JsonMapper.ToObject<Root>(dictionaryString);
			foreach (var item in root.Dictionaries.DictionaryItem)
			{
				string language = item.Language;

				if (language != currentLanguage)
				{
					continue;
				}
				foreach (var item1 in item.StringItem)
				{
					if (!localizationManager.AddRawString(item1.Key, item1.Value))
					{
						Log.Warning("Can not add raw string with key '{0}' which may be invalid or duplicate.", item1.Key);
						return false;
					}
				}
			}

			return true;
		}
		catch (Exception exception)
		{
			Log.Warning("Can not parse dictionary data with exception '{0}'.", exception.ToString());
			return false;
		}
	}
}
