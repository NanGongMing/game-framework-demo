﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constant
{
	public const string Language = "Setting.Language";
	public const string MusicMuted = "Setting.MusicMuted";
	public const string MusicVolume = "Setting.MusicVolume";
	public const string SoundEffectMuted = "Setting.SoundEffectMuted";
	public const string SoundEffectVolume = "Setting.SoundEffectVolume";
	public const string UISoundEffectMuted = "Setting.UISoundEffectMuted";
	public const string UISoundEffectVolume = "Setting.UISoundEffectVolume";


}
